package main

import (
	"fmt"
	"os"
	"os/user"

	"gitlab.com/krobolt/monkey/ast"
	"gitlab.com/krobolt/monkey/lexer"
	"gitlab.com/krobolt/monkey/parser"
	"gitlab.com/krobolt/monkey/repl"
)

func debug() {
	input := "foobar;"

	l := lexer.New(input)
	p := parser.New(l)
	program := p.ParserProgram()
	n := len(program.Statements)
	if n != 1 {
		fmt.Sprintf("expected 1 statement got %d", n)
	}
	fmt.Println(program.Statements[0])
	fmt.Println(program.Statements[0].(*ast.ExpressionStatement))

	fmt.Println(program.Statements[0].(*ast.ExpressionStatement))

	statement, ok := program.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		panic(fmt.Sprintf("expected *ast.Indenfier got: %T", statement.Expression))
	}
	ident, ok := statement.Expression.(*ast.Indentifier)
	if !ok {
		panic(fmt.Sprintf("exp not *ast.Identifier. got=%T", statement.Expression))
	}
	fmt.Println(ident)
}

func main() {
	debug()

	//something.m[0].(*ast.ExpressionStatement)

	//serv()
}

func serv() {
	user, err := user.Current()
	if err != nil {
		panic(err)
	}
	fmt.Println("Hello %s! This is the Monkey programming language!\n", user.Username)
	fmt.Println("Feel free to try some commands")
	repl.Start(os.Stdin, os.Stdout)

}
