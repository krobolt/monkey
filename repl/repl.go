package repl

import (
	"bufio"
	"fmt"
	"io"

	"gitlab.com/krobolt/monkey/lexer"
	"gitlab.com/krobolt/monkey/token"
)

const PROMT = ">> "

func Start(in io.Reader, out io.Writer) {
	scanner := bufio.NewScanner(in)

	for {
		fmt.Println(PROMT)
		sc := scanner.Scan()
		if !sc {
			return
		}
		line := scanner.Text()
		l := lexer.New(line)

		for tok := l.NextToken(); tok.Type != token.EOF; tok = l.NextToken() {
			fmt.Printf("%+v\n", tok)
		}
	}
}
